package control;


import java.util.Date;

import model.Database;
/**
 * 
 * This class handles all of the hardware interfaces. It's the link between the actual garage
 * and the model represented by the Database class.
 */
public class BikeGarageManager {
	BarcodePrinter printer;
	ElectronicLock entryLock;
	ElectronicLock exitLock;
	PinCodeTerminal terminal;
	private long lastFailedEntry, lastSuccessfulEntry;
	Database db;
	StringBuilder pinInput;

	/**
	 * Creates a BikeGarageManager
	 * @param Database db
	 * the database containing this BikeGarageManager
	 */
	public BikeGarageManager(Database db) {
		this.lastFailedEntry = System.currentTimeMillis();
		this.lastSuccessfulEntry = System.currentTimeMillis();
		this.db = db;
		this.pinInput = new StringBuilder();
	}

	/**
	 * 
	 * @param BarcodePrinter printer
	 * @param ElectronicLock entryLock
	 * @param ElectronicLockexitLock
	 * @param PinCodeTerminal terminal
	 * Register hardware so that BicycleGarageManager knows which drivers to
	 * access.
	 */
	public void registerHardwareDrivers(BarcodePrinter printer,
			ElectronicLock entryLock, ElectronicLock exitLock,
			PinCodeTerminal terminal) {
		this.printer = printer;
		this.entryLock = entryLock;
		this.exitLock = exitLock;
		this.terminal = terminal;
	}

	/**
	 * 
	 * @param String bicycleID
	 * Will be called when a user has used the bar code reader at the entry
	 * door. Bicycle ID should be a string of 5 characters, where every
	 * character can be '0', '1',... '9'.
	 */
	public void entryBarcode(String bicycleID) {
		if (db.getCurrentCapacity() < db.getMaxCapacity()) {
			if (db.verifyBarcode(bicycleID)) {
				db.findUser(bicycleID).addEntryTime(new Date());
				entryLock.open(20);
				db.setCurrentCapacity(db.getCurrentCapacity() + 1);
			}
		} else {
			terminal.lightLED(0, 5);
		}
	}

	/**
	 * Will be called when a user has used the bar code reader at the exit door.
	 * Bicycle ID should be a string of 5 characters, where every character can
	 * be '0', '1',... '9'.
	 */
	public void exitBarcode(String bicycleID) {
		if (db.verifyBarcode(bicycleID)) {
			db.findUser(bicycleID).addExitTime(new Date());
			exitLock.open(20);
			if (db.getCurrentCapacity() > 0) {
				db.setCurrentCapacity(db.getCurrentCapacity() - 1);
			}
		}
	}

	/**
	 * 
	 * @param char c
	 * Will be called when a user has pressed a key at the keypad at the entry
	 * door. The following characters could be pressed: '0', '1',... '9', '*',
	 * '#'.
	 */
	public void entryCharacter(char c) {
		long time = System.currentTimeMillis();
		if (time - lastFailedEntry > 20000 && time - lastSuccessfulEntry > 5000) {
			pinInput.append(c);
			if (pinInput.length() >= 4) {
				if (db.verifyPin(pinInput.toString())) {
					entryLock.open(20);
					terminal.lightLED(1, 5);
					lastSuccessfulEntry = System.currentTimeMillis();
				} else {
					terminal.lightLED(0, 20);
					lastFailedEntry = System.currentTimeMillis();
				}
				pinInput.delete(0, pinInput.length());
			}
		}
	}

	/**
	 * @param String barcode
	 * This function is called from the database when a barcode needs to be printed.
	 */
	public void printBarcode(String barcode) {
		printer.printBarcode(barcode);
	}
}
