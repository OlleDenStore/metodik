package control;

import model.Database;
import testDrivers.BarcodePrinterTestDriver;
import testDrivers.BarcodeReaderEntryTestDriver;
import testDrivers.BarcodeReaderExitTestDriver;
import testDrivers.ElectronicLockTestDriver;
import testDrivers.PinCodeTerminalTestDriver;
import view.MainWindow;

public class Main {

	/**
	 * The main function that starts the program.
	 * @param args
	 */
	public static void main(String[] args) {
		Database db = new Database();
		BikeGarageManager manager = new BikeGarageManager(db);
		db.registerBikeGarageManager(manager);
		ElectronicLock entryLock = new ElectronicLockTestDriver("Entry lock");
		ElectronicLock exitLock = new ElectronicLockTestDriver("Exit lock");
		BarcodePrinter printer = new BarcodePrinterTestDriver();
		PinCodeTerminal terminal = new PinCodeTerminalTestDriver();
		manager.registerHardwareDrivers(printer, entryLock, exitLock, terminal);
		terminal.register(manager);
		BarcodeReader readerEntry = new BarcodeReaderEntryTestDriver();
		BarcodeReader readerExit = new BarcodeReaderExitTestDriver();
		readerEntry.register(manager);
		readerExit.register(manager);
		MainWindow window = new MainWindow(db);
		window.main();
	}
}
