package view;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import model.Database;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

/**
 * This class creates an add window.
 * It can be used to add, find, remove a user to the system.
 * It can also be used to view and change the settings of the garage.
 *
 */
public class MainWindow {

	private JFrame frmBikeGarage;
	protected Database db;

	/**
	 * Launch the application.
	 */
	public void main() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainWindow.this.frmBikeGarage.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Draws the window connected to the database.
	 * 
	 * @param Database
	 */
	public MainWindow(Database db) {
		this.db = db;
		initialize();
	}

	private void initialize() {
		frmBikeGarage = new JFrame();
		frmBikeGarage.setResizable(false);
		frmBikeGarage.setTitle("Bike Garage ");
		frmBikeGarage.setBounds(100, 100, 450, 300);
		frmBikeGarage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBikeGarage.getContentPane().setLayout(null);
		
		JButton btnFind = new JButton("Find user");
		btnFind.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FindWindow ab = new FindWindow(db);
				ab.OpenFind();
			}
		});
		btnFind.setBounds(12, 94, 133, 60);
		frmBikeGarage.getContentPane().add(btnFind);
		
		JButton btnAdd = new JButton("Add user");
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AddWindow ab = new AddWindow(db);
				ab.OpenAdd();
			}
		});
		btnAdd.setBounds(154, 94, 133, 60);
		frmBikeGarage.getContentPane().add(btnAdd);
		
		JButton btnRemove = new JButton("Remove user");
		btnRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RemoveWindow ab = new RemoveWindow(db);
				ab.OpenRmv();
			}
		});
		btnRemove.setBounds(299, 94, 133, 60);
		frmBikeGarage.getContentPane().add(btnRemove);
		
		JLabel lblWelcomeChooseAn = new JLabel("Welcome! Choose an operation");
		lblWelcomeChooseAn.setFont(new Font("Dialog", Font.BOLD, 18));
		lblWelcomeChooseAn.setBounds(62, 12, 370, 70);
		frmBikeGarage.getContentPane().add(lblWelcomeChooseAn);
		
		JButton btnGarageOptions = new JButton("Garage options");
		btnGarageOptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GarageOptionsWindow ab = new GarageOptionsWindow(db);
				ab.OpenGOW();
			}
		});
		btnGarageOptions.setBounds(12, 166, 420, 95);
		frmBikeGarage.getContentPane().add(btnGarageOptions);
	}

}
