package view;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.WindowConstants;

import model.Database;
import model.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.regex.Pattern;

import javax.swing.JLabel;

/**
 * This class creates an add window.
 * It can be used to add a new user to the system.
 *
 */
public class AddWindow {
	
	private JFrame frmAdd;
	private JTextField textFieldAdd;
	private Database db;

	/**
	 * Launch the application.
	 */
	public void OpenAdd() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					AddWindow.this.frmAdd.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Draws the window connected to the database.
	 * 
	 * @param Database db
	 */
	public AddWindow(Database db) {
		this.db = db;
		initialize();
	}

	private void initialize() {
		frmAdd = new JFrame();
		frmAdd.setResizable(false);
		frmAdd.setTitle("Add");
		frmAdd.setBounds(100, 100, 450, 300);
		frmAdd.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frmAdd.getContentPane().setLayout(null);

		textFieldAdd = new JTextField();
		textFieldAdd.setBounds(65, 126, 325, 22);
		
		class AddWindowActionListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Pattern.matches(Database.ID_FORMAT, textFieldAdd.getText())) {
					User user = new User(textFieldAdd.getText(),"0000",db);
					if (db.addUser(user)) {
						db.updateBarcodeForUser(user);
						GenerateWindow ab1 = new GenerateWindow(db, user);
						ab1.OpenGenerate();
						frmAdd.dispose();
					} else {
						JOptionPane.showMessageDialog(null,
								"User already exists!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Wrong format!");
				}
			}
		}
		
		textFieldAdd.addActionListener(new AddWindowActionListener());
		frmAdd.getContentPane().add(textFieldAdd);
		textFieldAdd.setColumns(10);

		JButton btnAddAdd = new JButton("Confirm");
		btnAddAdd.addActionListener(new AddWindowActionListener());
		btnAddAdd.setBounds(65, 183, 97, 25);
		frmAdd.getContentPane().add(btnAddAdd);

		JButton btnCancelAdd = new JButton("Cancel");
		btnCancelAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frmAdd.dispose();
			}
		});
		btnCancelAdd.setBounds(293, 183, 97, 25);
		frmAdd.getContentPane().add(btnCancelAdd);

		JLabel lblNewLabel = new JLabel("Insert:");
		lblNewLabel.setBounds(65, 72, 70, 15);
		frmAdd.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel(
				"Personal number formatted: YYMMDD-XXXX");
		lblNewLabel_1.setBounds(65, 99, 325, 15);
		frmAdd.getContentPane().add(lblNewLabel_1);
	}
}
