package view;


import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import model.Database;
import model.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This class creates a Remove assurance window.
 * It can be used to assure that a user is to be removed from the system.
 *
 */
public class RemoveAssurance {

	private JFrame frame;
	private Database db;
	private User user;

	/**
	 * Launch the application.
	 */
	public  void OpenQRmv() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					RemoveAssurance.this.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Draws the window connected to the database and user.
	 * 
	 * @param Database db
	 * @param User user
	 */
	public RemoveAssurance(Database db, User user) {
		this.db = db;
		this.user = user;
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnYesRmv = new JButton("Yes");
		btnYesRmv.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "User " + user.getCitizenIdNumber() + " was successfully removed!");
				db.removeUser(user);
				frame.dispose();
			}
		});
		btnYesRmv.setBounds(82, 138, 97, 25);
		frame.getContentPane().add(btnYesRmv);
		
		JButton btnNoRmv = new JButton("No");
		btnNoRmv.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
					}
		});
		btnNoRmv.setBounds(264, 138, 97, 25);
		frame.getContentPane().add(btnNoRmv);
		
		JLabel lblAreYouSure = new JLabel("Are you sure you want to remove " + user.getCitizenIdNumber());
		lblAreYouSure.setBounds(82, 47, 350, 16);
		frame.getContentPane().add(lblAreYouSure);
		
		
		
		
	}
}
