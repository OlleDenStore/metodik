package view;


import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import model.Database;
import model.User;

/**
 * This class creates an Info window.
 * It can be used to view and change the information of the user.
 *
 */
public class InfoWindow {

	private JFrame frmInformation;
	private User user;
	private Database db;
	private JLabel feedback;

	/**
	 * Launch the application.
	 */
	public void OpenInfo() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					InfoWindow.this.frmInformation.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Draws the window connected to the database.
	 * @param Database db
	 * @param User user
	 */
	public InfoWindow(Database db, User user) {
		this.db=db;
		this.user = user;
		initialize();
	}

	private void initialize() {
		frmInformation = new JFrame();
		frmInformation.setResizable(false);
		frmInformation.setTitle("Information");
		frmInformation.setBounds(100, 100, 450, 391);
		frmInformation.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frmInformation.getContentPane().setLayout(null);
		
		JButton btnRegisterEntryTime = new JButton("Register entry time");
		btnRegisterEntryTime.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date d = new Date();
				db.setCurrentCapacity(db.getCurrentCapacity()+1);
				user.addEntryTime(d);
				feedback.setText("Registered entry time: "+d.toString());
			}
		});
		btnRegisterEntryTime.setBounds(63, 136, 280, 25);
		frmInformation.getContentPane().add(btnRegisterEntryTime);

		
		JButton btnRegisterExitTime = new JButton("Register exit time");
		btnRegisterExitTime.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date d = new Date();
				if(user.checkedIn()){
					db.setCurrentCapacity(db.getCurrentCapacity()-1);
				}
				user.addExitTime(d);
				feedback.setText("Registered exit time: "+d.toString());
			}
		});
		btnRegisterExitTime.setBounds(63, 173, 280, 25);
		frmInformation.getContentPane().add(btnRegisterExitTime);
		
		JButton btnGenerateBarcode = new JButton("Generate barcode");
		btnGenerateBarcode.setBounds(63, 247, 280, 25);
		frmInformation.getContentPane().add(btnGenerateBarcode);
		btnGenerateBarcode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String barcode = db.updateBarcodeForUser(user);
				feedback.setText("Generated barcode: "+barcode);
			}
	});

		JButton btnRemove = new JButton("Remove user");
		btnRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int yesNo = JOptionPane.showConfirmDialog(null, "Really remove user?", "Really remove user?", JOptionPane.YES_NO_OPTION);
				 if (yesNo == JOptionPane.YES_OPTION) {
					JOptionPane.showMessageDialog(null, "User removed successfully");
					db.removeUser(user);
					frmInformation.dispose();
				}
			}
		});
		btnRemove.setBounds(63, 284, 280, 25);
		frmInformation.getContentPane().add(btnRemove);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frmInformation.dispose();
				}
		});
		
		btnExit.setBounds(152, 321, 97, 25);
		frmInformation.getContentPane().add(btnExit);
		
		feedback = new JLabel("Welcome! Choose an option.");
		feedback.setBounds(12, 41, 420, 85);
		frmInformation.getContentPane().add(feedback);
		
		JButton btnViewCurrentBarcode = new JButton("View and print current barcode");
		btnViewCurrentBarcode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				feedback.setText("Current barcode is: " + user.getBarcode());
				db.printBarcode(user.getBarcode());
			}
		});
		btnViewCurrentBarcode.setBounds(63, 210, 280, 25);
		frmInformation.getContentPane().add(btnViewCurrentBarcode);
	}
}
