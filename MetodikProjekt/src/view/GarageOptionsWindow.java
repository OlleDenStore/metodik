package view;

import java.awt.EventQueue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import model.Database;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * This class creates a Garage options window.
 * It can be used to list all registered bikes, set PIN-code and set capacity.
 *
 */
public class GarageOptionsWindow {

	private JFrame frmGarageOptions;
	private Database db;
	private JTextField capField;
	private JTextField pinField;

	/**
	 * Launch the application.
	 */
	public void OpenGOW() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					GarageOptionsWindow.this.frmGarageOptions.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Draws the window connected to the database.
	 * 
	 * @param Database db
	 */
	public GarageOptionsWindow(Database db) {
		this.db=db;
		initialize();
	}

	private void initialize() {
		frmGarageOptions = new JFrame();
		frmGarageOptions.setTitle("Garage Options");
		frmGarageOptions.setResizable(false);
		frmGarageOptions.setBounds(100, 100, 500, 300);
		frmGarageOptions.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frmGarageOptions.getContentPane().setLayout(null);
		
		JButton btnGenerateListOf = new JButton("Generate list of bikes");
		btnGenerateListOf.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String daysString =JOptionPane.showInputDialog(
						"The List contains bikes that have been in the garage for how many days?");
				int days = 0;
				if (daysString != null) {
					try {
						days = 0- Integer.parseInt(daysString);
					}catch (NumberFormatException nfe){
						JOptionPane.showMessageDialog(null, "Not a number");
						return;
					}
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DATE, days);
					db.generateListOfBikes(cal.getTime());
					JOptionPane.showMessageDialog(null, "The list exist in the file ListOfBikes.txt",
							"The list exist in the file ListOfBikes.txt", JOptionPane.INFORMATION_MESSAGE);
				}

			} 
		});
		
		btnGenerateListOf.setBounds(119, 12, 232, 59);
		frmGarageOptions.getContentPane().add(btnGenerateListOf);
		
		JButton btnCancelGOW = new JButton("Cancel");
		btnCancelGOW.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frmGarageOptions.dispose();
			}
		});
		btnCancelGOW.setBounds(365, 236, 117, 25);
		frmGarageOptions.getContentPane().add(btnCancelGOW);		
		
		final JLabel pinLabel = new JLabel("Current PIN: "+db.getPin());
		pinLabel.setBounds(24, 100, 232, 25);
		frmGarageOptions.getContentPane().add(pinLabel);
		
		class PinListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Pattern.matches("\\d{4}", pinField.getText())) {
					db.setPin(pinField.getText());
					pinField.setText("");
					pinLabel.setText("Current PIN: "+db.getPin());
				} else {
					JOptionPane.showMessageDialog(null, "Error:Pin must be 4 digits");
				}
			}
		}
		
		JButton btnSetPinCode = new JButton("Set pin code");
		btnSetPinCode.addActionListener(new PinListener());
		btnSetPinCode.setBounds(355, 100, 127, 25);
		frmGarageOptions.getContentPane().add(btnSetPinCode);	
		
		pinField = new JTextField();
		pinField.setColumns(10);
		pinField.setBounds(282, 103, 55, 19);
		frmGarageOptions.getContentPane().add(pinField);
		pinField.addActionListener(new PinListener());
		
		
		capField = new JTextField();
		capField.setBounds(282, 152, 55, 19);
		frmGarageOptions.getContentPane().add(capField);
		capField.setColumns(10);
		
		final JLabel capLabel = new JLabel("Current/Max Capacity: "+db.getCurrentCapacity()+"/"+db.getMaxCapacity());
		capLabel.setBounds(24, 149, 240, 25);
		frmGarageOptions.getContentPane().add(capLabel);
		
		class CapacityListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Pattern.matches("\\d+", capField.getText())) {
					db.setMaxCapacity(Integer.parseInt(capField.getText()));
					capField.setText("");
					capLabel.setText("Current/Max Capacity: "+db.getCurrentCapacity()+"/"+db.getMaxCapacity());
				} else {
					JOptionPane.showMessageDialog(null, "Error:Invalid capacity");
				}
			}
		}
		
		JButton btnSetCapacity = new JButton("Set capacity");
		btnSetCapacity.addActionListener(new CapacityListener());
		capField.addActionListener(new CapacityListener());
		btnSetCapacity.setBounds(355, 149, 127, 25);
		frmGarageOptions.getContentPane().add(btnSetCapacity);

	}
}
