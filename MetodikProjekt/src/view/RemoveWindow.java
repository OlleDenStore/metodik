package view;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.WindowConstants;

import model.Database;
import model.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

/**
 * This class creates a Remove window.
 * It can be used to remove a user from the system. 
 *
 */
public class RemoveWindow {

	private JFrame frmRemove;
	private JTextField textFieldRemove;
	private Database db;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblInsertEither;

	/**
	 * Launch the application.
	 */
	public void OpenRmv() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					RemoveWindow.this.frmRemove.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Draws the window connected to the database.
	 * 
	 * @param Database db
	 */
	public RemoveWindow(Database db) {
		this.db = db;
		initialize();
	}

	private void initialize() {
		frmRemove = new JFrame();
		frmRemove.setResizable(false);
		frmRemove.setTitle("Remove");
		frmRemove.setBounds(100, 100, 450, 300);
		frmRemove.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frmRemove.getContentPane().setLayout(null);
		
		textFieldRemove = new JTextField();
		textFieldRemove.setBounds(65, 126, 327, 22);
		textFieldRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				User user = db.findUser(textFieldRemove.getText());
				if(user==null){
					JOptionPane.showMessageDialog(null, "User not found");
				}else{
					RemoveAssurance ab = new RemoveAssurance(db, user);
					ab.OpenQRmv();
					frmRemove.dispose();
				}
			}
		});
		frmRemove.getContentPane().add(textFieldRemove);
		textFieldRemove.setColumns(20);
		
		JButton btnRemoveRemove = new JButton("Remove");
		btnRemoveRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				User user = db.findUser(textFieldRemove.getText());
				if(user==null){
					JOptionPane.showMessageDialog(null, "User not found");
				}else{
					RemoveAssurance ab = new RemoveAssurance(db, user);
					ab.OpenQRmv();
					frmRemove.dispose();
				}
			}
		});
		btnRemoveRemove.setBounds(65, 183, 97, 25);
		frmRemove.getContentPane().add(btnRemoveRemove);
		
		JButton btnCancelRemove = new JButton("Cancel");
		btnCancelRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frmRemove.dispose();
					}
		});
		btnCancelRemove.setBounds(295, 183, 97, 25);
		frmRemove.getContentPane().add(btnCancelRemove);
		
		lblNewLabel = new JLabel("barcode formatted: XXXXX");
		lblNewLabel.setBounds(65, 99, 327, 15);
		frmRemove.getContentPane().add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Personal number formatted: YYMMDD-XXXX, or");
		lblNewLabel_1.setBounds(65, 72, 343, 15);
		frmRemove.getContentPane().add(lblNewLabel_1);
		
		lblInsertEither = new JLabel("Insert either:");
		lblInsertEither.setBounds(65, 45, 327, 15);
		frmRemove.getContentPane().add(lblInsertEither);
	}

}
