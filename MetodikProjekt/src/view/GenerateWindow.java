package view;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import model.Database;
import model.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This class creates a Generate window.
 * It can be used to generate a new barcode for the user added to the system.
 *
 */
public class GenerateWindow {

	private JFrame frmGenerate;
	private User user;
	private Database db;

	/**
	 * Launch the application.
	 */
	public void OpenGenerate() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					GenerateWindow.this.frmGenerate.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Draws the window connected to the database and user.
	 * 
	 * @param Database db
	 * @param User user
	 */
	public GenerateWindow(Database db, User user) {
		this.db = db;
		this.user = user;
		initialize();
	}

	private void initialize() {
		frmGenerate = new JFrame();
		frmGenerate.setResizable(false);
		frmGenerate.setTitle("Generate");
		frmGenerate.setBounds(100, 100, 450, 300);
		frmGenerate.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frmGenerate.getContentPane().setLayout(null);
		JButton btnGenerate = new JButton("Generate Code");
		btnGenerate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				user.addBarcode(db.updateBarcodeForUser(user));
				db.printBarcode(user.getBarcode());
				JOptionPane.showMessageDialog(null, "User " + user.getCitizenIdNumber() + " was added with the barcode " + user.getBarcode());
				frmGenerate.dispose();
			}
		});
		btnGenerate.setBounds(44, 92, 150, 54);
		frmGenerate.getContentPane().add(btnGenerate);
		
		JButton btnNewButton = new JButton("Cancel");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frmGenerate.dispose();
					}
		});
		btnNewButton.setBounds(242, 92, 150, 54);
		frmGenerate.getContentPane().add(btnNewButton);
	}

}
