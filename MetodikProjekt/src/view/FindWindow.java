package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import model.Database;
import model.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.regex.Pattern;

import javax.swing.JLabel;

/**
 * This class creates a find window.
 * It can be used to find a user in the system the system.
 *
 */
public class FindWindow {

	private JFrame frmFind;
	private JTextField textFieldFind;
	protected Database db;

	/**
	 * Launch the application.
	 */
	public void OpenFind() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					FindWindow.this.frmFind.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 *Draws the window connected to the database.
	 * 
	 * @param Database db
	 */
	public FindWindow(Database db) {
		this.db = db;
		initialize();
	}

	private void initialize() {
		frmFind = new JFrame();
		frmFind.setResizable(false);
		frmFind.setTitle("Find");
		frmFind.setBounds(100, 100, 450, 300);
		frmFind.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frmFind.getContentPane().setLayout(null);

		textFieldFind = new JTextField();
		textFieldFind.setBounds(65, 126, 325, 22);
		frmFind.getContentPane().add(textFieldFind);
		
		class FindWindowActionListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Pattern.matches(Database.BARCODE_FORMAT,
						textFieldFind.getText())
						|| Pattern.matches(Database.ID_FORMAT,
								textFieldFind.getText())) {
					User user = db.findUser(textFieldFind.getText());
					if (user == null) {
						JOptionPane.showMessageDialog(null, "User not found");
					} else {
						InfoWindow ab = new InfoWindow(db, user);
						ab.OpenInfo();

					}
				} else {
					JOptionPane.showMessageDialog(null, "Wrong format!");
				}
			}
		}
		textFieldFind.addActionListener(new FindWindowActionListener());
		textFieldFind.setColumns(10);

		JButton btnFindFind = new JButton("Find");
		btnFindFind.addActionListener(new FindWindowActionListener());
		btnFindFind.setBounds(63, 183, 97, 25);
		frmFind.getContentPane().add(btnFindFind);

		JButton btnCancelFind = new JButton("Cancel");
		btnCancelFind.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frmFind.dispose();
			}
		});

		JLabel label1 = new JLabel("Insert either:");
		label1.setBounds(67, 45, 108, 15);
		frmFind.getContentPane().add(label1);

		btnCancelFind.setBounds(293, 183, 97, 25);
		frmFind.getContentPane().add(btnCancelFind);

		JLabel label2 = new JLabel("barcode formatted: XXXXX");
		label2.setBounds(65, 99, 325, 15);
		frmFind.getContentPane().add(label2);

		JLabel label3 = new JLabel("Personal number formatted: YYMMDD-XXXX, or");
		label3.setBounds(65, 72, 367, 15);
		frmFind.getContentPane().add(label3);

	}
}
