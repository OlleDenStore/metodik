package model;

import java.util.Date;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

/**
 * This class models a user in the system.
 */
public class User {

	String citizenIdNumber;
	String barcode;
	Date entry, exit;
	Database db;

	/**
	 * Creates a user with IDnumber and a barcode.
	 * @param String citizenIdNumber
	 * The users unique citizen identification. In Sweden, the "personnummer".
	 * @param String barcode
	 * The barcode associated with the users bike.
	 * @param Database db
	 * The database containing this user.
	 */
	public User(String citizenIdNumber,String barcode,Database db) {
		this.citizenIdNumber = citizenIdNumber;
		this.barcode = barcode;
		entry = new Date();
		exit = new Date();
		this.db=db;
	}

	private User(String citizenIdNumber, String barcode, Date entry, Date exit,Database db) {
		this.citizenIdNumber = citizenIdNumber;
		this.barcode = barcode;
		this.entry = entry;
		this.exit = exit;
		this.db=db;
	}

	/**
	 * Adds a barcode to the user.
	 * @param String barcode
	 * The barcode to be added.
	 */
	public void addBarcode(String barcode){
		this.barcode=barcode;
		db.writeToFile();
	}
	/**
	 * Checks to see if this user's bike is currently stored in the garage.
	 * @return true if the bike is currently stored in the garage. Otherwise false.
	 */
	public boolean checkedIn(){
		return entry.compareTo(exit)>0;
	}
	
	/**
	 * Adds an entry time for when the bike was checked in.
	 * @param Date time
	 * The time used for entry time.
	 */
	public void addEntryTime(Date time){
		this.entry=time;
		db.writeToFile();
	}
	
	/**
	 * Adds an exit time for when the bike was checked out.
	 * @param Date time
	 * the time to be used for exit time.
	 */
	public void addExitTime(Date time){
		this.exit=time;
		db.writeToFile();
	}
	
	
	/**
	 * Returns a string with the IDnumber for the bike owner.
	 * @return String IDnumber
	 * The bike owner's IDnr.
	 */
	public String getCitizenIdNumber() {
		return citizenIdNumber;
	}

	/**
	 * Returns a string with barcode for the bike owner.
	 * @return String barcode
	 * bike owners barcode.
	 */
	public String getBarcode() {
		return barcode;
	}
	
	/**
	 * Returns entry time for when the owner last entered the garage.
	 * @return Date entry
	 * The user's latest entry time.
	 */
	public Date getEntry() {
		return entry;
	}

	/**
	 * Returns exit time for when the owner last exited the garage.
	 * @return Date exit
	 * The user's latest exit time.
	 */
	public Date getExit() {
		return exit;
	}

	/** Returns a string containing a users IDnumber, bar code, exit and entry time
	 * @return String
	 * A string with the IDnumber, barcode, exit and entry time. Each attribute separated
	 * by a # in the string.
	 */
	@Override
	public String toString() {
		return citizenIdNumber + "#" + barcode + "#" + entry.getTime() + "#"
				+ exit.getTime();
	}

	/**
	 * This method creates a new user from a String describing the user's attributes.
	 * @param String input
	 * The string describing the user. This is in the format described by the toString method.
	 * @param Database db
	 * The database to which this user will be added.
	 * @return User
	 * The created instance of User.
	 */
	public static User fromString(String input,Database db) {
		if (Pattern.matches(Database.ID_FORMAT + "#" + Database.BARCODE_FORMAT
				+ "#\\d+#\\d+", input)) {
			String[] parts = input.split("#");
			User user = new User(parts[0], parts[1], new Date(
					Long.parseLong(parts[2])), new Date(
					Long.parseLong(parts[3])),db);
			return user;
		} else {
			JOptionPane.showMessageDialog(null, "Error:Database corrupted");
			System.exit(0);
		}
		return null;
	}

}
