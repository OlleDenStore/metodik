package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import control.BikeGarageManager;

/**
 * 
 * This class handles the database for the garage. It keeps tracks of all the users
 * and what bikes are currently in the garage.
 */
public class Database {
	private String pin;
	private HashMap<String, User> usersById, usersByBarcode;
	private BikeGarageManager bgm;
	private int maxCapacity, currentCapacity;
	public static final String BARCODE_FORMAT = "\\d{5}";
	public static final String ID_FORMAT = "\\d{6}-\\d{4}";


	/**
	 * Creates a new database.
	 */
	public Database() {
		super();
		this.usersById = new HashMap<String, User>();
		this.usersByBarcode = new HashMap<String, User>();
		readDatabaseFile();
	}

	private void readDatabaseFile() {
		Scanner in = null;
		try {
			in = new Scanner(new File("database.txt"));
		} catch (FileNotFoundException e) {
			JOptionPane
					.showMessageDialog(null,
							"No database file found. New database created with default pin:1234, capacity:100");
			this.pin = "1234";
			this.maxCapacity=100;
			return;
		}
		String input=in.nextLine();
		if(!Pattern.matches("\\d+", input)){
			JOptionPane.showMessageDialog(null, "Error:Database corrupted");
		}
		this.pin = input;
		input=in.nextLine();
		if(!Pattern.matches("\\d+", input)){
			JOptionPane.showMessageDialog(null, "Error:Database corrupted");
		}
		this.maxCapacity = Integer.parseInt(input);
		while (in.hasNext()) {
			addUser(User.fromString(in.nextLine(),this));
		}
		this.currentCapacity = checkCapacity();
		in.close();

	}

	/**
	 * Verify that the barcode exist in the database.
	 * @param String barcode
	 * barcode to be verified.
	 * @return boolean
	 * True if the barcode exist in the database.
	 * False if the barcode doesn't exist in the database.
	 */
	public boolean verifyBarcode(String barcode) {
		if (usersByBarcode.containsKey(barcode)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Verify that the pincode is the correct pincode.
	 * @param String pin
	 * the pincode to be verified.
	 * @return boolean
	 * True if the pincode is the correct one.
	 * False if the pincode is not the correct one.
	 */
	public boolean verifyPin(String pin) {
		if (this.pin.equals(pin)) {
			return true;
		} else {
			return false;
		}
	}
	
	/** Returns the current pin code.
	 * @return String pin
	 * The current pin code for the garage.
	 */ 
	public String getPin() {
		return pin;
	}

	/** Sets a new pin code.
	 * @param String pin
	 * the new pin code to be set.
	 */	
	public void setPin(String pin) {
		this.pin = pin;
		writeToFile();
	}

	/** Adds a user in the database.
	 * @param User user
	 * The user to be added.
	 * @return boolean
	 * True if new user is added.
	 * False if a user with that id-number already exists.
	 */
	public boolean addUser(User user) {
		if (!usersById.containsKey(user.getCitizenIdNumber())) {
			usersById.put(user.getCitizenIdNumber(), user);
			usersByBarcode.put(user.getBarcode(), user);
			writeToFile();
			return true;
		}
		return false;
	}

	/** Removes a user from the database.
	 * @param User user
	 * The user to be removed.
	 */
	public void removeUser(User user) {
		usersById.remove(user.getCitizenIdNumber());
		usersByBarcode.remove(user.getBarcode());
		writeToFile();
	}

	private String generateFileOutput() {
		StringBuilder output = new StringBuilder();
		String newL = System.getProperty("line.separator");
		output.append(pin);
		output.append(newL);
		output.append(maxCapacity);
		output.append(newL);
		Set<Entry<String, User>> users = usersById.entrySet();
		for (Entry<String, User> entry : users) {
			output.append(entry.getValue().toString());
			output.append(newL);
		}
		return output.toString();
	}

	/**Returns the current capacity.
	 * @return int capacity
	 * The current capacity of the garage.
	 */
	public int checkCapacity() {
		Collection<User> users = usersById.values();
		int cap = 0;
		for (User user : users) {
			if (user.checkedIn()) {
				cap++;
			}
		}
		return cap;
	}

	/**
	 * Writes the entire database to the file database.txt
	 */
	public void writeToFile() {
		PrintWriter out = null;
		try {
			out = new PrintWriter("database.txt");
		} catch (FileNotFoundException e) {
			System.out.println("Error! File can not be created");
			System.exit(1);
		}
		out.println(generateFileOutput());
		out.close();
	}

	/** Updates the bar code for a user.
	 * @param User user
	 * The user for which bar code is to be updated.
	 * @return String newBar
	 * The updated barcode for the user.
	 */
	public String updateBarcodeForUser(User user) {
		usersByBarcode.remove(user.getBarcode());
		String newBar = generateNewBarcode();
		user.addBarcode(newBar);
		usersByBarcode.put(newBar, user);
		return newBar;
	}

	private String generateNewBarcode() {
		String barcode = generateBarcode();
		while (usersByBarcode.containsKey(barcode)) {
			barcode = generateBarcode();
		}
		return barcode;
	}

	private String generateBarcode() {
		Random rand = new Random();
		int newBarcode = rand.nextInt(100000);
		String bar = "" + (newBarcode + 100000);
		return bar.substring(bar.length() - 5, bar.length());
	}

	/**
	 * Returns a user matching the input.
	 * @param String text
	 * The personal id-number of the user or the barcode.
	 * for the users bike.
	 * @return User
	 * the user matching the string.
	 */
	public User findUser(String text) {
		if (usersById.containsKey(text)) {
			return usersById.get(text);
		}
		if (usersByBarcode.containsKey(text)) {
			return usersByBarcode.get(text);
		}
		return null;
	}

	/**
	 * Prints the barcode.
	 * @param String barcode
	 * The barcode to be printed.
	 */
	public void printBarcode(String barcode) {
		bgm.printBarcode(barcode);
	}

	/**
	 * @param BikeGarageManager bgm
	 * The bikeGarageManager associated with this garage. Used in order to print barcodes.
	 */
	public void registerBikeGarageManager(BikeGarageManager bgm) {
		this.bgm = bgm;
	}

	/**
	 * @return int maxCapcity
	 * The max capacity for the garage.
	 */
	public int getMaxCapacity() {
		return maxCapacity;
	}

	/**
	 * @return int currentCapcity
	 * The current capacity for the garage.
	 */
	public int getCurrentCapacity() {
		return currentCapacity;
	}

	/**
	 * @param int currentCapacity
	 * Called by functions that change the current capacity of the garage. By for example
	 * entering an entry time for a user.
	 */
	public void setCurrentCapacity(int currentCapacity) {
		this.currentCapacity = currentCapacity;
	}
	
	/**
	 * @param int maxCapacity
	 * Changes the max capacity of the garage to maxCapacity.
	 */
	public void setMaxCapacity(int maxCapacity) {
		this.maxCapacity = maxCapacity;
		writeToFile();
	}
	
	/**
	 * @param Date date
	 * This function generates a list of all bikes stored in the garage where the
	 * entry date is earlier than date. The list is written to the file ListOfBikes.txt.
	 */
	public void generateListOfBikes(Date date) {
		PrintWriter out = null;
		try {
			out = new PrintWriter("ListOfBikes.txt");
		} catch (FileNotFoundException e) {
			System.out.println("Error! File can not be created");
			System.exit(1);
		}
		for (Map.Entry<String, User> entry : usersById.entrySet()) {
				if (entry.getValue().checkedIn() && entry.getValue().getEntry().compareTo(date) <= 0) {
					out.println(entry.getValue().toString());
				}
		}
		out.close();
	}
}
